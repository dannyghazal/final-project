﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class gameManager : MonoBehaviour
{
    public static int scoreValue = 0;
    Text score;

    // Start is called before the first frame update
    void Start()
    {
        score = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        score.text = "Score: " + scoreValue;

        if(scoreValue == 100)
        {
            SceneManager.LoadScene("gameOver");
        }

        if(Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
    public void play()
    {
        SceneManager.LoadScene("gameplay");
    }

}
