﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class enemyController : MonoBehaviour
{
    public float speed;
    private Transform target;
    public Rigidbody2D rbZombie;
    public Rigidbody2D rbPlayer;
    public GameObject blood;

    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);

        transform.LookAt(target.position);
        transform.Rotate(new Vector3(0, 90, 90), Space.Self);
    }
    private void OnTriggerEnter2D(Collider2D col)
    {

        if (col.gameObject.tag == "bullet")
        {
            Instantiate(blood, transform.position, Quaternion.identity);
            Destroy(rbZombie.gameObject);
            gameManager.scoreValue += 1;
        }
    }
}
