﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class soundManager : MonoBehaviour
{
    public static AudioClip shoot, enemyDeath, backgroundMusic;
    static AudioSource audioSrc;

    // Start is called before the first frame update
    void Start()
    {
        shoot = Resources.Load<AudioClip>("shoot");
        enemyDeath = Resources.Load<AudioClip>("zombie");
        backgroundMusic = Resources.Load<AudioClip>("music");

        audioSrc = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
       
    }
    public static void playsound (string clip)
    {
        switch(clip)
        {
            case "shoot":
                audioSrc.PlayOneShot(shoot);
                break;

            case "zombie":
                audioSrc.PlayOneShot(enemyDeath);
                break;

            case "music":
                audioSrc.PlayOneShot(backgroundMusic);
                break;
        }
    }
}
