﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zombieSpawner : MonoBehaviour
{
    public Transform[] zombieSpawns;
    public GameObject zombie;
    int randomZombieSpawn;
    public static bool spawnAllowed;

    // Start is called before the first frame update
    void Start()
    {
        spawnAllowed = true;
        InvokeRepeating("SpawnAZombie", 0f, 1f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SpawnAZombie()
    {
        if(spawnAllowed)
        {
            randomZombieSpawn = Random.Range(0, zombieSpawns.Length);
            Instantiate(zombie, zombieSpawns[randomZombieSpawn].position, Quaternion.identity);
        }
    }
}
